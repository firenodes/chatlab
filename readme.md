# ChatLab

Yet another voxel game engine.

## Project Structure

This repository contains several projects:

- **core** - The common code of the game engine, which contains the *entity* api and the event system.
- **server** - The server-sided implementation for the engine
- **client** - The client-sided implementation for the engine - contains *rendering* calls.

## Experimental

This is currently an experimental project, therefore the api may change at any time.
