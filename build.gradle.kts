plugins {
    kotlin("jvm") version "1.5.0"
    application
}

sourceSets.main {
    java.srcDirs("src/main/java", "src/main/kotlin")
}

repositories {
    mavenLocal()
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.spring.io/milestone")
}

dependencies {
    implementation("org.graalvm.sdk:graal-sdk:21.1.0")
    implementation("org.graalvm.truffle:truffle-api:21.1.0")
    implementation("dev.throwouterror:event-bus:1.1.1")
    implementation("info.laht.threekt:core:r1-ALPHA-27")
    implementation("io.projectreactor.netty:reactor-netty-core:1.0.7")
    implementation("io.projectreactor.netty:reactor-netty-http:1.0.7")
    implementation("dev.throwouterror:toe-utils:0.0.1")
    implementation("com.google.code.gson:gson:2.8.6")
}

application {
    mainClass.set("com.theoparis.chatlab.Main")
}

java.sourceCompatibility = JavaVersion.VERSION_11
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.suppressWarnings = true
    kotlinOptions.jvmTarget = "11"
}
