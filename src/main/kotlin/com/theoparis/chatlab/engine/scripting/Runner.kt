package com.theoparis.chatlab.engine.scripting

import dev.throwouterror.eventbus.SimpleEventBus
import dev.throwouterror.eventbus.event.Event
import dev.throwouterror.util.data.json
import org.graalvm.polyglot.Context
import org.graalvm.polyglot.PolyglotException
import org.graalvm.polyglot.Source
import org.graalvm.polyglot.Value
import java.io.File
import java.io.IOException

/**
 * The runner for the scripting engine.
 * This is an experimental feature; the api may change at any time.
 */
open class Runner(protected val eventBus: SimpleEventBus = SimpleEventBus()) {
    companion object {
        @JvmStatic
        val languages = mutableMapOf(
            "js" to ".js",
            "python" to ".py",
            "ruby" to ".rb"
        )
    }

    open fun createContext(lang: String = "js"): Context {
        System.setProperty("polyglot.js.nashorn-compat", "true")
        // TODO: better security
        val context = Context.newBuilder(lang)
            .allowAllAccess(true)
            .allowHostClassLookup {
                true
            }
            .build()
        context.polyglotBindings.putMember("events", this.eventBus)
        return context
    }

    fun runScript(args: MutableList<String>): Boolean {
        val src = args.joinToString { " " }
        val ctx = createContext()
        val lang = src.split(".").getOrNull(1) ?: "js"

        eventBus.fireEvent(
            Event(
                "beforeRun",
                json {
                    it.addProperty("language", lang)
                }
            )
        )

        try {
            val result: Value = if (args[0].isNotEmpty() && args[0].endsWith(languages.getOrDefault("js", ".js"))) {
                try {
                    val file = File(args[0])
                    if (!file.exists()) {
                        println("Could not read script file ${args[0]}")
                        return false
                    }
                    ctx.eval(Source.newBuilder(lang, file).build())
                } catch (e: IOException) {
                    e.printStackTrace()
                    return false
                }
            } else
                ctx.eval(lang, src)
            if (result.isString)
                println(result.asString())
        } catch (ex: PolyglotException) {
            println("Error: ${ex.message}")
            ex.printStackTrace()
            return false
        }

        return true
    }
}
