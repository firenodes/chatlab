package com.theoparis.chatlab.engine.core

import dev.throwouterror.util.data.JsonUtils
import dev.throwouterror.util.math.Tensor

data class Transform(
    var position: Tensor = Tensor(0, 0, 0),
    var rotation: Tensor = Tensor(0, 0, 0),
    var scale: Tensor = Tensor(1, 1, 1)

)

object TransformUtils {
    fun Tensor.toJson() = JsonUtils.builder.toJsonTree(this).asJsonObject
    fun Transform.toJson() = JsonUtils.builder.toJsonTree(this).asJsonObject
}
