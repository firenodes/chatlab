package com.theoparis.chatlab.engine.core

import com.google.gson.JsonObject
import dev.throwouterror.util.data.JsonUtils

/**
 * Create a world with the given properties.
 * The name property must be set, otherwise it will cause errors.
 */
open class World(val properties: JsonObject) {
    private val entities = mutableListOf<Entity>()

    /**
     * Gets the state of this world as a json object.
     * This state can then be serialized to a string, or sent via an event bus.
     */
    fun getState(): JsonObject = JsonUtils.builder.toJsonTree(this).asJsonObject

    fun getEntities() = entities.toList()

    open fun update() {
        entities.forEach {
            it.update()
        }
    }

    open fun addEntity(entity: Entity) = entities.add(entity)

    companion object {
        @JvmStatic
        /**
         * Loads a world from a state object.
         * The state object can be parsed from a string.
         */
        fun fromState(state: JsonObject) = JsonUtils.builder.fromJson(state, World::class.java)
    }
}
