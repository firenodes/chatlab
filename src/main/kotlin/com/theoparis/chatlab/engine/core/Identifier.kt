package com.theoparis.chatlab.engine.core

import kotlin.random.Random

object Identifier {
    private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
    const val maxLength = 32

    @JvmStatic
    fun generate() = (1..maxLength)
        .map { Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("")
}
