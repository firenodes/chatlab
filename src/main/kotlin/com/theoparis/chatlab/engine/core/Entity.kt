package com.theoparis.chatlab.engine.core

import com.google.gson.JsonObject
import dev.throwouterror.eventbus.SimpleEventBus
import dev.throwouterror.util.data.JsonUtils
import dev.throwouterror.util.math.Tensor

open class Entity(val type: String) {
    protected val transform = Transform()
    val events = SimpleEventBus()
    val id = Identifier.generate()

    var position: Tensor = transform.position
        set(value) {
            transform.position = value
            field = value
        }
        get() = transform.position

    var rotation: Tensor = transform.rotation
        set(value) {
            transform.rotation = value
            field = value
        }
        get() = transform.rotation
    var scale: Tensor = transform.scale
        set(value) {
            transform.scale = value
            field = value
        }
        get() = transform.scale

    fun toJson(): JsonObject = JsonUtils.builder.toJsonTree(this).asJsonObject

    /**
     * Called when the game is updated each frame.
     */
    open fun update() {}
}
