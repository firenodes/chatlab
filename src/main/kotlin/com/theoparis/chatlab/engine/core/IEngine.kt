package com.theoparis.chatlab.engine.core

import dev.throwouterror.eventbus.EventBus
import dev.throwouterror.eventbus.event.Event

interface IEngine {
    fun run(args: MutableList<String>)
    fun getEventBus(): EventBus<Event>
}
