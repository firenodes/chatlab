package com.theoparis.chatlab.engine.client

import com.google.gson.JsonObject
import com.theoparis.chatlab.engine.client.entity.ClientEntity
import com.theoparis.chatlab.engine.core.Entity
import com.theoparis.chatlab.engine.core.World

class ClientWorld(properties: JsonObject) : World(properties) {
    fun getClientEntities() = getEntities().filterIsInstance<ClientEntity>()

    override fun addEntity(entity: Entity): Boolean {
        return super.addEntity(entity)
    }
}
