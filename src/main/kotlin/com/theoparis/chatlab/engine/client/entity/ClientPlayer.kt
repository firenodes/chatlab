package com.theoparis.chatlab.engine.client.entity

import com.theoparis.chatlab.engine.client.ClientEngine
import com.theoparis.chatlab.engine.client.PlayerControls
import com.theoparis.chatlab.engine.client.util.CubeModel
import info.laht.threekt.cameras.AbstractCamera
import info.laht.threekt.cameras.PerspectiveCamera

class ClientPlayer(engine: ClientEngine) : ClientEntity("player") {
    var camera: AbstractCamera = PerspectiveCamera(75, engine.window.aspect, 0.1, 1000).apply {
        position.z = 5f
    }
    val controls = PlayerControls(camera, engine.window)

    init {
        model = CubeModel(transform)
    }

    override fun update() {
        super.update()
        controls.update()
    }
}
