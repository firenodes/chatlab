package com.theoparis.chatlab.engine.client.util

import dev.throwouterror.util.math.Tensor
import info.laht.threekt.math.Vector3

object VectorUtils {
    fun Tensor.copyFrom(vec: Vector3) = this.set(vec.x, vec.y, vec.z)
    fun Vector3.copyFrom(tensor: Tensor) = this.set(tensor.x, tensor.y, tensor.z)
}
