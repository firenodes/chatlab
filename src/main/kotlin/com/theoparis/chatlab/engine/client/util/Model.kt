package com.theoparis.chatlab.engine.client.util

import com.google.gson.JsonObject
import com.theoparis.chatlab.engine.core.Transform
import dev.throwouterror.util.data.JsonUtils.merge
import dev.throwouterror.util.data.json
import info.laht.threekt.core.Object3D
import info.laht.threekt.core.Object3DImpl
import info.laht.threekt.geometries.BoxBufferGeometry
import info.laht.threekt.materials.MeshBasicMaterial
import info.laht.threekt.objects.Mesh
import java.lang.Exception

abstract class Model(val transform: Transform, val properties: JsonObject) {
    var obj: Object3D? = null
        protected set

    abstract fun generate()
}

class EmptyModel(transform: Transform, properties: JsonObject? = JsonObject()) : Model(
    transform,
    json {
        it.addProperty("type", "cube")
    }.merge(properties!!)
) {
    override fun generate() {
        obj = Object3DImpl()
    }
}

class CubeModel(transform: Transform, properties: JsonObject? = JsonObject()) : Model(
    transform,
    json {
        it.addProperty("type", "cube")
    }.merge(properties!!)
) {
    override fun generate() {
        obj = Mesh(
            BoxBufferGeometry(this.transform.scale.x, this.transform.scale.y, this.transform.scale.z),
            MeshBasicMaterial().also {
                try {
                    it.color.set(this.properties.get("color").asInt)
                } catch (_e: Exception) {
                    // Default to white color
                    it.color.set(0xffffff)
                }
            }
        )
    }
}
