package com.theoparis.chatlab.engine.client.entity

import com.theoparis.chatlab.engine.client.util.Model
import com.theoparis.chatlab.engine.core.Entity

open class ClientEntity(type: String) : Entity(type) {
    lateinit var model: Model
        protected set

    fun onModelChange() {
        model.generate()
    }

    override fun update() {
        super.update()
        model.obj?.position?.set(transform.position.x, transform.position.y, transform.position.z)
        model.obj?.rotation?.set(transform.rotation.x, transform.rotation.y, transform.rotation.z)
        model.obj?.scale?.set(transform.scale.x, transform.scale.y, transform.scale.z)
    }
}
