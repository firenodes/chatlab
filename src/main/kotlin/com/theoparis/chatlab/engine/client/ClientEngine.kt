package com.theoparis.chatlab.engine.client

import com.google.gson.JsonObject
import com.theoparis.chatlab.engine.client.entity.ClientPlayer
import com.theoparis.chatlab.engine.client.util.OperatingSystem
import com.theoparis.chatlab.engine.core.IEngine
import com.theoparis.chatlab.engine.scripting.Runner
import dev.throwouterror.eventbus.EventBus
import dev.throwouterror.eventbus.event.Event
import info.laht.threekt.Window
import info.laht.threekt.WindowClosingCallback
import info.laht.threekt.math.Color
import info.laht.threekt.renderers.GLRenderer
import info.laht.threekt.scenes.Scene
import org.graalvm.polyglot.Context

class ClientEngine : Runner(), IEngine {
    lateinit var window: Window
        private set
    private lateinit var scene: Scene
    private lateinit var world: ClientWorld

    override fun createContext(lang: String): Context {
        val ctx = super.createContext(lang)
        // TODO: world class with game objects list
        ctx.polyglotBindings.putMember("world", this)
        return ctx
    }

    override fun run(args: MutableList<String>) {
        OperatingSystem.loadNatives()
        Window(antialias = 4).use { window ->
            this.window = window
            this.scene = Scene().apply {
                setBackground(Color.black)
            }
            this.world = ClientWorld(JsonObject())

            val renderer = GLRenderer(window.size)
            world.addEntity(
                ClientPlayer(this).also {
                    it.model.obj?.let { object3D -> scene.add(object3D) }
                }
            )

            eventBus.fireEvent(Event("gameSetup", JsonObject()))

            // val clock = Clock()
            val player = world.getClientEntities().filterIsInstance<ClientPlayer>()[0]

            window.animate {
                // val dt = clock.getDelta()

                /*eventBus.fireEvent(
                    Event(
                        "gameLoop",
                        json {
                            it.addProperty("deltaTime", dt)
                        }
                    )
                )*/

                world.update()

                // Collision

                renderer.render(scene, player.camera)
            }
            window.onCloseCallback = WindowClosingCallback {
                player.controls.dispose()
            }
        }
    }

    override fun getEventBus(): EventBus<Event> {
        return this.eventBus
    }
}
