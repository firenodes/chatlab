package com.theoparis.chatlab.engine.client.util

import java.io.File
import java.util.*


object OperatingSystem {
    private val system = System.getProperty("os.name").lowercase(Locale.getDefault())
    val osForLWJGLNatives: String
        get() {
            if (system.startsWith("win")) {
                return "windows"
            }
            if (system.startsWith("mac")) {
                return "macosx"
            }
            if (system.startsWith("lin")) {
                return "linux"
            }
            return if (system.startsWith("sol")) {
                "solaris"
            } else "unknown"
        }

    fun loadNatives() {
        val nativePath = "${System.getProperty("user.dir")}${File.separator}natives"
        println("Loading natives at $nativePath")
        System.setProperty(
            "org.lwjgl.librarypath",
            nativePath
        )
    }
}
