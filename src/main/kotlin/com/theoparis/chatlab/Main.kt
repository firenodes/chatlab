package com.theoparis.chatlab

import com.theoparis.chatlab.engine.client.ClientEngine

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val engine = ClientEngine()
//        engine.runScript(mutableListOf("scripts/test.js"))
        engine.run(args.toMutableList())
    }
}
